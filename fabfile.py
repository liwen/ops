from fabric.api import *
from fabric.contrib.files import *
from fabric.operations import put

from lib.hello import *
from lib.init import *
from lib.cube import *

from local_settings import *

# To quiet all interactive
#DEBIAN_FRONTEND=noninteractive

env.hosts = [
    #"cube0.tuijianbao.net",
    #"cube1.tuijianbao.net",
    #"cube2.tuijianbao.net",
    #"cube10.tuijianbao.net",
    #"linode.sunliwen.com",
    #"ec2-175-41-212-197.ap-northeast-1.compute.amazonaws.com",
    #"www.gdian.com:988",
    #"ec2-175-41-224-146.ap-northeast-1.compute.amazonaws.com",
    #"cube11.tuijianbao.net",
    #"cube12.tuijianbao.net",
    "cube13.tuijianbao.net",

    #"ydhd01.tuijianbao.net",
    #"chinacache1.tuijianbao.net",
    #"hichina1.tuijianbao.net",
    #"cube3.tuijianbao.net",
    #"cube1",
    #"cube2",
    #"cube3"

    #"sunliwen.com"
]

#env.user = u"cube"
#env.password = "tuijianbao4u"


def host_type():
    run('uname -a')

def init_all():
    init_sys()
    init_nmp()

def init_apps():
    init_app(url="sunliwen.com")
    init_app(url="i.sunliwen.com")
    init_app(url="pi.sunliwen.com")
    init_app(url="m.sunliwen.com")

def init_sys():
    install_screen()
    set_locale()
    #set_apt_cacher()  # not all site need this.
    apt_get_update()
    install_add_apt_repository()
    install_general_packages()

def init_apt_cacher():
    set_locale()
    install_apt_cacher()
    enable_apt_cacher()
    start_apt_cacher()
    set_apt_cacher()
    apt_get_update()

def init_java():
    add_jdk_source()
    apt_get_update()
    install_jdk()

def add_jdk_source():
    sudo('apt-add-repository "deb http://archive.canonical.com/ `lsb_release -cs` partner"')

def install_jdk():
    sudo('yes "yes" | DEBIAN_FRONTEND=readline apt-get -q -y install sun-java6-jdk')

def shutdown():
        sudo("halt -n")

def reboot():
        sudo("reboot -n")

def shell(command=None):
    run(command)

def install_screen():
    sudo("apt-get -q -y install screen")

def install_language_pack_en_base():
    sudo("apt-get -y -q install language-pack-en-base")

def push_gutenberg():
    put(local_path="gutenberg.tar.gz",
        remote_path="/tmp/gutenberg.tar.gz")
    sudo("tar zxvf /tmp/gutenberg.tar.gz")

def init_nmp():
    install_nginx()
    install_mysql()
    install_php()
    init_phpfpm()

def install_php():
    sudo("apt-get -y -q --force-yes install php5-cli php5-cgi php5-mysql php5-curl")
    sudo("apt-get -y -q --force-yes install php5-mcrypt libmcrypt4 mcrypt")
    sudo("apt-get -y -q --force-yes install php5-gd")
    sed("/etc/php5/conf.d/mcrypt.ini","#",";",use_sudo=True) # php5-mcrypt still use old # comment, sed to ;
    # phpmyadmin have to be interactive
def install_pma():
    sudo("apt-get -y -q --force-yes install phpmyadmin")

def install_mysql():
    sudo("yes 'yes' | DEBIAN_FRONTEND=readline apt-get -y -q install mysql-server mysql-server-5.1")

def install_pymysql():
    sudo("yes 'yes' | DEBIAN_FRONTEND=readline apt-get install libmysqlclient16 libmysqlclient16-dev python-dev")

def install_nginx():
    sudo("add-apt-repository ppa:nginx/stable")
    sudo("apt-get update")
    sudo("apt-get -y -q install nginx-full nginx-common")
    # hide server token
    sed("/etc/nginx/nginx.conf","server_tokens on;","server_tokens off;",use_sudo=True)

def ulimit():
    sed("/etc/rc.local","exit 0","#exit 0", use_sudo=True)
    sudo("echo 'ulimit -HSn 65536' >> /etc/rc.local")
    sudo("echo 'ulimit -m unlimited' >> /etc/rc.local")
    sudo("echo 'ulimit -v unlimited' >> /etc/rc.local")

def init_phpfpm():
    # TODO: use official fpm when available
    sudo("add-apt-repository ppa:brianmercer/php")
    #sudo("add-apt-repository ppa:zerkalica/php5-fpm")
    #sudo("add-apt-repository ppa:nginx/php5")
    sudo("apt-get update")
    sudo("apt-get -y -q install php5-fpm")
    # GOTCHA: post_max_size = 8M
    # /etc/php5/fpm/php.ini
    # Use Socket instead of TCP
    sed("/etc/php5/fpm/php5-fpm.conf","^;listen = /var/run/php5-fpm.sock","listen = /dev/shm/php5-fpm.sock",use_sudo=True)
    sed("/etc/php5/fpm/php5-fpm.conf","^listen = 127.0.0.1:9000",";listen = 127.0.0.1:9000",use_sudo=True)
    # Hide PHP Version
    sed("/etc/php5/fpm/php.ini","^expose_php = On","expose_php = Off",use_sudo=True)
    # Restart php5-fpm
    sudo("/etc/init.d/php5-fpm restart")

def install_hg():
    sudo("apt-get -q -y install mercurial")
    # pip usually provides newer version
    sudo("pip install --upgrade mercurial")

def init_uwsgi():
    install_uwsgi()
    # TODO, auto config app

def install_gunicorn():
    sudo("apt-add-repository ppa:bchesneau/gunicorn")
    sudo("apt-get update")
    sudo("apt-get -q -y install gunicorn")

def install_pip():
    sudo("apt-get -q -y install python-dev python-pip")
    sudo("pip install --upgrade pip")
    sudo("mv /usr/local/bin/pip /usr/local/bin/pip-0.3")
    sudo("ln -s /usr/local/bin/pip-2.6 /usr/local/bin/pip")

def install_supervisor():
    sudo("apt-get -q -y install supervisor")
    #sudo("pip install --upgrade supervisor")  # sometime supervisor from pip will break init.d script, so stay with deb version

def wajig_hold_upgrade():
    sudo("apt-get -y install wajig")
    # hold dangerous packages from upgrading
    sudo("wajig hold linux-headers-server linux-image-server linux-server grub-common grub-pc ")
    sudo("apt-get -y upgrade")

def install_uwsgi():
    cmds = ["add-apt-repository ppa:uwsgi/release",
            "apt-get update",
            "apt-get -q -y install uwsgi-python libxml2-dev",
            "pip install --upgrade uwsgi"]
    for cmd in cmds:
        sudo(cmd)


def install_ntp():
    # ntp to keep clusters time sync
    sudo("apt-get -y install ntp")

def install_munin():
    sudo("apt-get -q -y install munin")

def install_munin_nodes():
    sudo("apt-get -q -y install munin-common munin-node")

def install_redis():
    put(local_path="apt/dotdeb.org.list",
        remote_path="/tmp/dotdeb.org.list")
    sudo("mv /tmp/dotdeb.org.list /etc/apt/sources.list.d/")
    #sudo("wget -q -O - http://www.dotdeb.org/dotdeb.gpg | sudo apt-key add")
    sudo("apt-get update")
    sudo("apt-get -q -y --force-yes install redis-server")

def install_mongodb_10gen():
    sudo("apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10")
    put(local_path="apt/10gen.list",
        remote_path="/tmp/10gen.list")
    sudo("mv /tmp/10gen.list /etc/apt/sources.list.d/")
    sudo("apt-get update")
    sudo("apt-get install mongodb-10gen")


def init_py():
    install_pip()
    install_hg()
    install_pymysql()
    install_pymongo()
    install_pyredis()
    install_tornado()
    install_django()
    install_trac()


def killall(name=None):
    if not name:
        print "process name"
    else:
        sudo("killall %s" % name)

def apt_get_clean():
    sudo("apt-get clean")


def install_pymongo():
    sudo("apt-get -q -y install python-dev")
    sudo("pip install --upgrade pymongo") # c extension to promote performance
    #sudo("pip install --upgrade pymongo")

def install_pyredis():
    sudo("pip install --upgrade redis") # c extension to promote performance

def install_tornado():
    sudo("pip install --upgrade tornado asyncmongo")

def install_django():
    sudo("pip install --upgrade django")

def install_trac():
    sudo("pip install --upgrade trac")
    sudo("pip install --upgrade mysql-python")

def put_pubkey():
    put(local_path="~/.ssh/id_rsa.pub",
        remote_path="/tmp/id_rsa.pub")

def init_service(name=None):
    if name:
        sudo("mkdir -p /cube/service/"+name+"/log")
        sudo("mkdir -p /cube/service/"+name+"/temp")
        sudo("mkdir -p /cube/service/"+name+"/work")
        #sudo("cd /cube/service/"+name)
        #sudo("hg clone http://i.tuijianbao.net/hg/server")

def install_ufw():
    sudo("apt-get -q -y install ufw")

def enable_ufw():
    # allow 22 first, then enable
    sudo("ufw allow 22")
    sudo("ufw allow 988")
    sudo("ufw allow 80")
    sudo("ufw allow 81")
    #sudo("ufw allow 8000")
    #sudo("ufw allow from 192.168.0.0/24")
    sudo("yes 'Y' | ufw enable")

def install_apc():
    # gzip -dc /usr/share/doc/php-apc/apc.php.gz > apc.php
    sudo("apt-get install php-apc")
    sed("/etc/php5/fpm/conf.d/apc.ini", "","",use_sudo=True)
