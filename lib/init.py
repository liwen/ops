from fabric.api import *
from fabric.contrib.files import *
from fabric.operations import put

def set_locale():
    sudo('apt-get -q -y install language-pack-en-base')
    put(local_path='locale',
        remote_path='/tmp/locale')
    sudo('mv /tmp/locale /etc/default/locale')
    sudo('dpkg-reconfigure locales')

def install_apt_cacher():
    sudo("yes 'YES' | apt-get install apt-cacher")

def set_apt_cacher():
    put(local_path='90-apt-proxy.conf',
            remote_path='/tmp/90-apt-proxy.conf')
    sudo('mv /tmp/90-apt-proxy.conf /etc/apt/apt.conf.d/90-apt-proxy.conf')

def enable_apt_cacher():
    # /etc/default/apt-cacher
    # AUTOSTART=1
    sed("/etc/default/apt-cacher", "AUTOSTART=0", "AUTOSTART=1", limit="", use_sudo=True, backup='.bak')

def start_apt_cacher():
    sudo("/etc/init.d/apt-cacher restart")

def apt_get_update():
        sudo('apt-get update')

def install_add_apt_repository():
        sudo("apt-get -q -y install python-software-properties")

def install_general_packages():
    sudo("apt-get -q -y install curl")

def init_app(url=None):
    if not url:
        print "Give the app a url"
        return
    sudo("mkdir -p /cube/app/%s/public_html" % url)
    sudo("mkdir -p /cube/app/%s/log" % url)
    sudo("chown -R www-data:www-data /cube/app/%s" % url)

    # init app nginx configuration file
    tmp_path = ("/tmp/%s" % url)
    available_path = ("/etc/nginx/sites-available/%s" % url)
    enabled_path = ("/etc/nginx/sites-enabled/%s" % url)

    local("sed -e 's/DOMAIN/%s/g' templates/nginx/DOMAIN.conf > /tmp/%s" % (url, url))
    put(local_path=tmp_path,
        remote_path=tmp_path)
    if exists(available_path):
        sudo("mv %s %s.bak" % (available_path, available_path))
    sudo("mv %s %s" % (tmp_path, available_path))
    if exists(enabled_path):
        sudo("rm %s" % enabled_path)
    sudo("ln -s %s %s" % (available_path, enabled_path))
    sudo("/etc/init.d/nginx restart")
