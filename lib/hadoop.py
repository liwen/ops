def add_cloudera_source():
        sudo('apt-add-repository "deb http://archive.cloudera.com/debian lucid-cdh3 contrib"')
        sudo('apt-add-repository "deb-src http://archive.cloudera.com/debian lucid-cdh3 contrib"')

def install(package=None):
        if not package:
                package = prompt("package name:")
        sudo('yes = "yes" | DEBIAN_FRONTEND=readline apt-get -q -y install %s' % package)

def add_cloudera_key():
        run("curl -s http://archive.cloudera.com/debian/archive.key | sudo apt-key add -")
        sudo("apt-get update")

def install_hadoop():
    sudo("apt-get -q -y install hadoop-0.20")

def purge_hadoop():
    sudo("apt-get -q -y purge hadoop-0.20")

def get_hadoop_conf():
    pass

def push_hadoop_conf():
    #sudo("cp -r /etc/hadoop-0.20/conf.empty /etc/hadoop-0.20/conf.cube")
    local("tar czvf hadoop-conf.cube.tar.gz conf.cube/")
    put(local_path='hadoop-conf.cube.tar.gz',
        remote_path='/tmp/hadoop-conf.cube.tar.gz')
    sudo("tar zxvf /tmp/hadoop-conf.cube.tar.gz")

    sudo("cp -R conf.cube /etc/hadoop-0.20/")
    sudo("rm -rf conf.cube")
    sudo("rm -rf hadoop-conf.cube.tar.gz")

def init_hadoop_folders():

    sudo("update-alternatives --install /etc/hadoop-0.20/conf hadoop-0.20-conf /etc/hadoop-0.20/conf.cube 50")

    sudo("rm -rf /cube/data/hadoop")
    sudo("rm -rf /cube/cache/hadoop")
    sudo("rm -rf /cube/logs/hadoop")

    sudo("mkdir -p /cube/data/hadoop/dfs/nn")
    sudo("mkdir -p /cube/data/hadoop/dfs/dn")
    sudo("mkdir -p /cube/cache/hadoop/mapred")
    sudo("mkdir -p /cube/cache/hadoop/hdfs")
    sudo("mkdir -p /cube/cache/hadoop/users")

    sudo("chown -R hdfs:hadoop /cube/data/hadoop/")
    sudo("chown -R hdfs:hadoop /cube/cache/hadoop/users")
    sudo("chown -R mapred:hadoop /cube/cache/hadoop/mapred")

    sudo("mkdir -p /cube/logs/hadoop/")
    sudo("mkdir -p /cube/logs/hadoop/")
    sudo("chown root:hadoop /cube/logs/hadoop/")
    sudo("chmod 775 /cube/logs/hadoop/")

def update_hadoop_default():
    commands = [
        "update-rc.d hadoop-0.20-namenode defaults",
        "update-rc.d hadoop-0.20-jobtracker defaults",
        "update-rc.d hadoop-0.20-secondarynamenode defaults",
        "update-rc.d hadoop-0.20-tasktracker defaults",
        "update-rc.d hadoop-0.20-datanode defaults"
    ]
    for command in commands:
        sudo(command)

def install_hadoop_slave():
    sudo("apt-get -q -y install hadoop-0.20-datanode")
    sudo("apt-get -q -y install hadoop-0.20-tasktracker")
    
    sudo("update-rc.d hadoop-0.20-tasktracker defaults")
    sudo("update-rc.d hadoop-0.20-datanode defaults")

def install_cloudera_master():
    install("hadoop-0.20 hadoop-0.20-namenode hadoop-0.20-secondarynamenode hadoop-0.20-jobtracker hadoop-0.20-tasktracker hadoop-0.20-datanode")

def install_cloudera_slave():
    install("hadoop-0.20 hadoop-0.20-datanode hadoop-0.20-tasktracker")

def service(name="datanode", command="start"):
    return ("/etc/init.d/hadoop-0.20-%s %s" % (name, command))

def start_datanode():
    sudo("/etc/init.d/hadoop-0.20-datanode start")

def start_namenode():
    sudo("/etc/init.d/hadoop-0.20-namenode start")
    sudo("/etc/init.d/hadoop-0.20-secondarynamenode start")

def start_mapred():
    sudo("/etc/init.d/hadoop-0.20-jobtracker start")
    sudo("/etc/init.d/hadoop-0.20-tasktracker start")

def master(operation="start"):
    names = ["namenode", "secondarynamenode", "datanode", "jobtracker", "tasktracker"]
    for name in names:
        sudo(service(name, operation))

def slave(operation="start"):
    names = ["datanode","tasktracker"]
    if operation == "stop":
        print "reverse stop"
        names.reverse()
    for name in names:
        sudo(service(name, operation))

def push_hadoop_cube_tar():
    local("tar czvf hadoop.tar.gz hadoop-0.20.2")
    put(local_path="hadoop.tar.gz",
        remote_path="/tmp/hadoop.tar.gz")
    sudo("tar zxvf /tmp/hadoop.tar.gz")
    sudo("rm /cube/hadoop -rf")
    sudo("mv hadoop-0.20.2 /cube/hadoop")
    sudo("chown hdfs:hadoop /cube/hadoop -R")

def clean_cube():
    sudo("rm /cube/* -Rf")

def init_hadoop_cube():
    sudo("mkdir -p /cube/local/hadoop")
    sudo("mkdir -p /cube/logs/hadoop")
    sudo("mkdir -p /cube/data/hadoop")
    
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")
    sudo("chown hdfs:hadoop /cube/logs/hadoop -R")
    sudo("chown hdfs:hadoop /cube/data/hadoop -R")

def push_hadoop_cloudera_tar_remote():
    sudo("tar zxvf /home/hadoop/hadoop-0.20.2-cdh3u0.tar.gz")
    sudo("rm /cube/local/hadoop -rf")
    sudo("mkdir -p /cube/local")
    sudo("mv hadoop-0.20.2-cdh3u0 /cube/local/hadoop")
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")

    sudo("rm /cube/data/hadoop -rf")
    sudo("mkdir -p /cube/data/hadoop")
    sudo("chown hdfs:hadoop /cube/data/hadoop -R")

def push_hadoop_cloudera_tar():
    local("tar czvf hadoop.tar.gz hadoop-0.20.2-cdh3u0")
    put(local_path="hadoop.tar.gz",
        remote_path="/tmp/hadoop.tar.gz")
    sudo("tar zxvf /tmp/hadoop.tar.gz")
    sudo("rm /cube/local/hadoop -rf")
    sudo("mkdir -p /cube/local")
    sudo("mv hadoop-0.20.2-cdh3u0 /cube/local/hadoop")
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")

    sudo("rm /cube/data/hadoop -rf")
    sudo("mkdir -p /cube/data/hadoop")
    sudo("chown hdfs:hadoop /cube/data/hadoop -R")

def push_hadoop_cloudera_conf():
    local("tar czvf hadoop_conf.tar.gz hadoop-0.20.2-cdh3u0/conf")
    put(local_path="hadoop_conf.tar.gz",
        remote_path="/tmp/hadoop_conf.tar.gz")
    sudo("tar zxvf /tmp/hadoop_conf.tar.gz")
    sudo("rm /cube/local/hadoop/conf -rf")
    sudo("mv hadoop-0.20.2-cdh3u0/conf /cube/local/hadoop")
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")

def push_cloudera_conf_to_remote():
    local("tar czvf hadoop_conf.tar.gz hadoop-0.20.2-cdh3u0/conf")
    put(local_path="hadoop_conf.tar.gz",
        remote_path="/tmp/hadoop_conf.tar.gz")
    sudo("tar zxvf /tmp/hadoop_conf.tar.gz")
    sudo("mv /cube/local/hadoop/conf /tmp/conf.bak -f")
    sudo("mv hadoop-0.20.2-cdh3u0/conf /cube/local/hadoop/conf")
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")

def push_hadoop_cube_conf():
    local("tar czvf hadoop_conf.tar.gz hadoop-0.20.2/conf")
    put(local_path="hadoop_conf.tar.gz",
        remote_path="/tmp/hadoop_conf.tar.gz")
    sudo("tar zxvf /tmp/hadoop_conf.tar.gz")
    sudo("rm /cube/local/hadoop/conf -rf")
    sudo("mv hadoop-0.20.2/conf /cube/local/hadoop")
    sudo("chown hdfs:hadoop /cube/local/hadoop -R")

def stop_hadoop():
    run("sudo -u hdfs /cube/local/hadoop/bin/stop-all.sh")

def start_hadoop():
    run("sudo -u hdfs /cube/local/hadoop/bin/start-dfs.sh")
    run("sudo -u hdfs /cube/local/hadoop/bin/start-mapred.sh")

def hadoop_namenode_format():
    #run("yes 'Y' | sudo -u hdfs /cube/local/hadoop/bin/hadoop namenode -format")
    run("yes 'Y' | sudo -u hdfs hadoop namenode -format")
    
def install_hbase():
    sudo("apt-get -y install hadoop-hbase")
    limits = [
        "hdfs - nofile 32768",
        "hbase - nofile 32768"
    ]
    limits_conf = "/etc/security/limits.conf"
    for limit in limits:
        if not contains(limit, limits_conf):
            sudo("echo %s >> %s" % (limit, limits_conf))

    pam = "session required pam_limits.so"
    pam_file = "/etc/pam.d/common-session"
    if not contains(pam, pam_file):
        sudo("echo %s >> %s " % (pam, pam_file))

    local("tar czvf hbase_conf.tar.gz hbase_conf")
    put(local_path="hbase_conf.tar.gz",
        remote_path="/tmp/hbase_conf.tar.gz")
    sudo("tar zxvf /tmp/hbase_conf.tar.gz")
    sudo("rm -rf /etc/hbase/conf")
    sudo("mv hbase_conf /etc/hbase/conf")

def install_hbase_master():
    install_hbase()
    sudo("apt-get install hadoop-hbase-master")
    sudo("update-rc.d hadoop-hbase-master defaults")

def install_hbase_region():
    install_hbase()
    sudo("apt-get install hadoop-hbase-regionserver")
    sudo("apt-get install hadoop-hbase-thrift")

    sudo("update-rc.d hadoop-hbase-regionserver defaults")
    sudo("update-rc.d hadoop-hbase-thrift defaults")

def install_zookeeper():
    sudo("rm -rf /cube/zookeeper")
    sudo("mkdir -p /cube/zookeeper")
    sudo("apt-get -q -y install hadoop-zookeeper-server")
    sudo("update-rc.d hadoop-zookeeper-server defaults")
    put(local_path="zoo.cfg",
        remote_path="/tmp/zoo.cfg")
    sudo("mv -f /tmp/zoo.cfg /etc/zookeeper/zoo.cfg")