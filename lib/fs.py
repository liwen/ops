
def install_lvm2():
    sudo("apt-get -q -y install lvm2")    

def init_cube():
    sudo("mkdir /cube")
    init_xvda()
    sudo("echo '/dev/xvda /cube ext4 noatime,data=writeback,barrier=0,nobh,errors=remount-ro 0 1' >> /etc/fstab")
    sudo("mount --all")
    
def init_xvda():
    sudo("mkfs.ext4 /dev/xvda")
    

def move_var_to_cube():
    #sudo("cd /var && find . -depth -print0 | cpio --null --sparse -pvd /cube")
    
    sudo("mv /var /var1")
    sudo("mkdir /var")
    sudo("echo '/cube /var bind defaults,bind 0 0' >> /etc/fstab")
    sudo("mount --all")

def create_lvm():
    commands = [
        "vgcreate fileserver /dev/xvda1",
        "lvcreate --name polygon --size 32G fileserver",
         "lvcreate --name cube -l 100%FREE fileserver",
        "mkfs.ext4 /dev/fileserver/cube",
        "mkdir /cube",
        "mkdir /polygon",
         'echo "/dev/fileserver/cube /cube ext4 noatime,data=writeback,barrier=0,nobh,errors=remount-ro 0 1" >> /etc/fstab',
         'echo "/dev/fileserver/polygon /polygon ext4 noatime,data=writeback,barrier=0,nobh,errors=remount-ro 0 1" >> /etc/fstab',
        "mount -a",
    ]
    for command in commands:
        sudo(command)
