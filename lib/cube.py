from fabric.api import * 
from fabric.contrib.files import *
from fabric.operations import put

from lib.init import *

@hosts("cube10.tuijianbao.net")
def init_api():
    init_app(url="api.tuijianbao.net")
    sudo("cd /cube/app/api.tuijianbao.net")
    sudo("hg clone http://liwen:viewsonic@i.tuijianbao.net/hg/server")